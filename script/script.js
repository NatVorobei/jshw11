function showPassword(){

    let firstBtn = document.getElementById('first_btn');
    let secondBtn = document.getElementById('second_btn');

    let submit = document.querySelector('.btn');

    let firstInput = document.getElementById('first_input');
    let secondInput = document.getElementById('second_input');
    
    let error = document.getElementById('error');

    firstBtn.addEventListener('click', () => {
        
        if(firstInput.getAttribute('type') === 'password'){
            firstInput.setAttribute('type', 'text');
            firstBtn.classList.remove('fa-eye-slash');
        } else {
            firstInput.setAttribute('type', 'password');
            firstBtn.classList.add('fa-eye-slash');
        }
    });

    secondBtn.addEventListener('click', () => {
        
        if(secondInput.getAttribute('type') === 'password'){
            secondInput.setAttribute('type', 'text');
            secondBtn.classList.remove('fa-eye-slash');
        } else {
            secondInput.setAttribute('type', 'password');
            secondBtn.classList.add('fa-eye-slash');
        }
    });

    submit.addEventListener('click', (e) => {
        e.preventDefault();
        if(firstInput.value === secondInput.value){
            alert('You are welcome!');
        } else {
            error.style.display = 'block';
        }
    })

}

showPassword();